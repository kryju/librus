# README #

**LibrusDownloader** to skrypt do pobierania załączników z wiadomości w systemie [Librus](https://synergia.librus.pl/).



### Uwagi ###

Testowany tylko na Chrome. I klasie 1B ;)

Librus jest bytem, który w każdej chwili może sie zmienić, a sam skrypt może przestać działać. 

Nie biorę odpowiedzialności za jego błędne działanie i ewentualne szkody jakie mógł spowodować ;) 

Skrypt nie rozwiązuje problemów z tokenami - jest to błąd serwera na który nie mam wpływu.

Kod jest otwarty - tak wiec można go modyfikować do woli.



### Jak to zainstalowac? ###

* W pierwszej kolejności potrzebny jest pluggin [Tempermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=pl)  
    ![Screenshot](img/InstallTampermonkey.png)

##  

* Nastepnie wystarczy kliknąć na link ze skryptem:  
    https://bitbucket.org/kryju/librus/raw/master/src/LibrusDownloader.user.js  

##  

* I nacisnąć przycisk 'Install':  
    ![Screenshot](img/InstallScript.png)

##  


* **Bardzo ważne** - przy pierwszym uruchomieniu należy się upewnić czy przeglądarka nie blokuje wyskakujących okienek  

##  


### Działanie ###

Po poprawnym zainstalowaniu skryptu, na stronach wiadomości systemu Librus pojawi sie nam dodatkowy przycisk 'Pobierz':

![Screenshot](img/DownloadButton.png)


### Usunięcie ###

Może się zdarzyć, iż po aktualizacji w systemie Librus dodatek przestanie działać. Jeżeli jest dostępna nowa wersja skryptu można go pobrać za pomoca przycisku 'Update' w Tampermonkey. 
Jeżeli nie ma aktualizacji, skrypt należy wyłączyć w ustawieniach:  

![Screenshot](img/SettingsTampermonkey1.png)  

##  

![Screenshot](img/SettingsTampermonkey2.png)  

##  

![Screenshot](img/SettingsTampermonkey3.png)

 
Bądź też usuwając kompletnie plugin Tampermonkey z Chrome.


