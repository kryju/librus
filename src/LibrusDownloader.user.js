// ==UserScript==
// @name         Librus downloader
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  try to take over the world!
// @author       You
// @match        https://synergia.librus.pl/wiadomosci/1/5/*
// @match        https://sandbox.librus.pl/index.php?action=CS*
// @grant        none
// ==/UserScript==


var POPUP_WINODWS_MONITORING = true; //in case of failures with download after Librus updates - try to deactivate this option.
var ATTACHMENT_REQUESTS_PERIODICITY = 3000; // every 3 sec request for next attachment
var MAX_RETRY_DOWNLOAD_CHECK = 16; //16 checks for download resutls
var DOWNLOAD_CHECK_INTERVAL = 1000; // 1sec


var progressState = 0;
var progressBar = 0;
var failureCounter = 0;
var okCounter = 0;
var retryCounter = 0;
var $ = window.jQuery; //var $ = window.$;


(function() {
    'use strict';

    var isMainWindow = (window.location.href.search("synergia.librus.pl/wiadomosci")>=0);

    if (isMainWindow)
    {
        InitializeMessageWindow();
    }
    else
    {
        if (POPUP_WINODWS_MONITORING)
        {
            InitializeDownloadWindow();
        }
    }

})();

function InitializeDownloadWindow()
{
    if (window.location.href.search("CSDownloadFailed") == -1)
    {
        retryCounter = 0;

        var checkSuccess = function() {
            var successObj = document.querySelectorAll(".success")[0];
            if (successObj.style.length>0)
            {
                window.opener.postMessage("OK", 'https://synergia.librus.pl');

                setTimeout(function(){
                    window.close();
                }, 2000);
            }
            else
            {
                retryCounter++;
                if (retryCounter < MAX_RETRY_DOWNLOAD_CHECK)
                {
                    setTimeout(checkSuccess, DOWNLOAD_CHECK_INTERVAL);
                }
            }
        }
        setTimeout(checkSuccess, DOWNLOAD_CHECK_INTERVAL);
    }
    else
    {
        window.opener.postMessage("FAIL", 'https://synergia.librus.pl');

        setTimeout(function(){
            window.close();
        }, 2000);
    }
};

function InitializeMessageWindow()
{
    var filePaths = [];

    var btnBack = document.querySelector("input[value='Wróć']");

    var btnDownload = document.createElement("input");
    btnDownload.type = "button";
    btnDownload.value = "Pobierz";
    btnDownload.onclick = function() { OnDownloadButtonClick(); };
    btnBack.parentElement.appendChild(btnDownload);

    var modalLoading = document.createElement("DIV");
    modalLoading.innerHTML = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog"> <div class="modal-dialog">     <div class="modal-content">    <div class="modal-header">    <h4  class="modal-title"><div id="idProgressState">Prosze czekac...</div></h4> </div> <div class="modal-body"> <div class="progress"> <div id="idProgressBar" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px"></div></div></div></div></div></div>';
    modalLoading.style.visibility = "hidden";
    btnBack.parentElement.appendChild(modalLoading);
    progressState = document.getElementById("idProgressState");
    progressBar = document.getElementById("idProgressBar");



    function OnDownloadButtonClick()
    {
        var modalLoading = document.getElementById("pleaseWaitDialog");
        modalLoading.style.visibility = "visible";

        failureCounter = 0;
        okCounter = 0;

        if (POPUP_WINODWS_MONITORING)
        {
            InitializePopUpListener();
            ProgressUpdate("Prosze czekac...");
        }

        setTimeout(function() {

            GetFilesToDownload();
            StartDownloading();

        }, 1000);
    }

    function GetFilesToDownload()
    {
        filePaths = [];

        var elAttachmetns = document.querySelectorAll("img[src='/assets/img/homework_files_icons/download.png']");
        for (let attachment of elAttachmetns)
        {
            var src = attachment.parentElement.innerHTML;
            var idxStart = src.indexOf("wiadomosci");
            var idxStop = src.indexOf(",\n");
            if ( (idxStart>=0) && (idxStop>=0) )
            {
                var imgPath = "https:\/\/synergia.librus.pl\/"+src.substr(idxStart, idxStop-idxStart-6);
                filePaths.push(imgPath);
            }
        }
    };

    function StartDownloading()
    {
        var idxPaths = 0;

        ProgressUpdate("Prosze czekac...   Do pobrania: " + filePaths.length);

        for (idxPaths=0; idxPaths<filePaths.length;idxPaths++)
        {
            //initialize download periodically
            setTimeout(function(idxPaths) {
                DownloadFile(idxPaths);
            }.bind(this, idxPaths), idxPaths * ATTACHMENT_REQUESTS_PERIODICITY);
        }

        if (!POPUP_WINODWS_MONITORING)
        {
            setTimeout(function() {
                var modalLoading = document.getElementById("pleaseWaitDialog");
                modalLoading.style.visibility = "hidden";
            }, filePaths.length * ATTACHMENT_REQUESTS_PERIODICITY);
        }
    }

    function DownloadFile(idxPaths)
    {
        if (idxPaths < filePaths.length)
        {
            window.open(filePaths[idxPaths], filePaths[idxPaths], 'width=500,height=400,resizable=yes,scrollbars');
        }
        else
        {

        }
    }

    function InitializePopUpListener()
    {
        window.addEventListener('message', event => {
            // IMPORTANT: check the origin of the data!
            if (event.origin.startsWith('https://sandbox.librus.pl')) {
                // The data was sent from your site.
                // Data sent with postMessage is stored in event.data:

                if (event.data == "OK")
                {
                    okCounter++;
                }
                if (event.data == "FAIL")
                {
                    failureCounter++;
                }

                // if all downloaded
                if (okCounter + failureCounter == filePaths.length)
                {
                    ProgressUpdate("Zakonczono         Do pobrania: " + filePaths.length + "    Potwierdzone: " + okCounter + "    Bledy: " + failureCounter);
                    var progressBar = document.getElementById("idProgressBar");
                    progressBar.style.visibility = "hidden";
                }
                else
                {
                    ProgressUpdate("Prosze czekac...   Do pobrania: " + filePaths.length + "    Potwierdzone: " + okCounter + "    Bledy: " + failureCounter);
                }
            } else {
                // The data was NOT sent from your site!
                // Be careful! Do not use it. This else branch is
                // here just for clarity, you usually shouldn't need it.
                return;
            }
        });
    }
};


function ProgressUpdate(state)
{
    progressState.innerHTML = state;
};
